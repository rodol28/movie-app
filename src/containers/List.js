import React, { Component, Fragment } from 'react'
import Card from '../components/card/Card';

const API = process.env.API;

class List extends Component {

    constructor() {
        super();
        this.state = {
            data: [],
            searchTerm: '',
            error: '',
            loading: true
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    async componentDidMount() {
        // const res = await fetch('../../assets/data.json');

        const res = await fetch(`${API}&s=batman`);
        const resJSON = await res.json();
        this.setState({data: resJSON.Search, loading: false});
    }

    async handleSubmit(e) {
        e.preventDefault();
        if(!this.state.searchTerm) {
            return this.setState({error: 'Please, write a valid text'});
        }

        const res = await fetch(`${API}&s=${this.state.searchTerm}`);
        const data = await res.json();
        console.log(data);

        if(!data.Search) {
            return this.setState({error: 'Movie not found!'});
        }
        this.setState({data: data.Search, error: '', searchTerm: ''});
    }

    render() {

        const { data, loading } = this.state;
        if(loading) {
            return <div className="text-white">Loading...</div>
        }

        return (
            <Fragment>
                <div className="col-md-4 offset-md-4 p-4">
                    <form onSubmit={this.handleSubmit}>
                        <input 
                            type="text" 
                            className="form-control"
                            placeholder="Search"
                            onChange={e => this.setState({searchTerm: e.target.value})}
                            autoFocus
                            value={this.state.searchTerm}
                        />
                    </form>
                    <p className="text-white" >{this.state.error ? this.state.error : ''}</p>
                </div>
                <div className="row">
                {
                    data.map(movie => {
                        return <Card movie = {movie} key = {movie.imdbID}/>
                    })
                }
            </div>
            </Fragment>
        );
    }
}

export default List;